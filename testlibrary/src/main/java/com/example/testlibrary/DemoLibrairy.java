package com.example.testlibrary;

import android.content.Context;
import android.widget.Toast;

public class DemoLibrairy {

    public static void toast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
